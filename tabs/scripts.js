const SHOW_TAB_EVENT = new Event("show_tab");
const HIDE_TAB_EVENT = new Event("hide_tab");

document.querySelectorAll(".button-tab").forEach((tab) => {
    tab.addEventListener("click", (event) => {
        document.querySelectorAll(".button-tab").forEach((button) => {
            button.classList.remove("active")
        });
        // Recupération de la liste de classes
        const classList = event.target.classList;
        // Recupération du second élément de la list de classe qui est l'id de la div à afficher
        const targetId = classList[1];
        // Appel de la function openTargetTab(...) avec l'id recupérer dans les classes
        closeCurrentOpenedTabIfNot(targetId);
        openTargetTab(targetId);
        event.target.classList.add("active")
    });
});

// Sleection de tout les élément qui corresponde au selecteur css ".tab-content"
// Pour chacun d'entre eux on ajoute un event listener
document.querySelectorAll(".tab-content").forEach((div) => {
    div.addEventListener("show_tab", (event) => {
        // On affiche la div ou on la cache en fonction de l'état précedent
        // div.hidden = !div.hidden
        div.classList.toggle("closed");
    });
    div.addEventListener("hide_tab", (event) => {
        // cache la div
        // div.hidden = true
        div.classList.add("closed");
    });
});

function closeCurrentOpenedTabIfNot(idTarget) {
    // document.querySelectorAll(".tab-content.opened)").forEach((div) => {
    document.querySelectorAll(".tab-content:not(.closed):not(#" + idTarget + ")").forEach((div) => {
        div.dispatchEvent(HIDE_TAB_EVENT);
    });
}

function openTargetTab(idTarget) {
    // Récup de l'élément d'id idTarget
    const div = document.getElementById(idTarget)
    // lancement de l'event SHOW_TAB_EVENT 
    div.dispatchEvent(SHOW_TAB_EVENT)
}

