export let currentPlayer = "tic";
export const TIC_CLASSNAME = "tic";
export const TAC_CLASSNAME = "tac";

export function displayPlayer() {
    document.querySelector("#nomJoueur").textContent = currentPlayer;
}

export function changePlayer() {
    if (currentPlayer === "tic") {
        currentPlayer = "tac";
    } else {
        currentPlayer = "tic";
    }
}

export function getPlayerClassName() {
    return currentPlayer === "tac" ? TAC_CLASSNAME : TIC_CLASSNAME;
}