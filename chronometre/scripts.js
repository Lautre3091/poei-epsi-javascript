let idChrono = null;
let secondes = 0;
let dizainesDeSecondes = 0;
let minutes = 0;
const secondesSpan = document.getElementById("secondes");
const minutesSpan = document.getElementById("minutes");
const startButton = document.getElementById("startButton");
const stopButton = document.getElementById("stopButton");
const resetButton = document.getElementById("resetButton");

startButton.addEventListener("click", () => startChrono());
stopButton.addEventListener("click", (event) => {
    console.log(event)
    stopChrono()
});
resetButton.addEventListener("click", () => clearChrono())

updateView(secondes, dizainesDeSecondes, minutes);

function startChrono(){
    idChrono = setInterval(runChrono, 1000);
    stopButton.hidden = false
    startButton.hidden = true
}

function stopChrono() {
    clearInterval(idChrono);
    stopButton.hidden = true
    startButton.hidden = false
}

function runChrono(){
    secondes++;
    if(secondes > 9){
        secondes = 0;
        dizainesDeSecondes++;
    }
    
    if(dizainesDeSecondes > 5){
        dizainesDeSecondes = 0;
        minutes++;
    }
    updateView(secondes, dizainesDeSecondes, minutes)
}

function clearChrono() {
    stopChrono()
    secondes = 0;
    dizainesDeSecondes = 0;
    minutes = 0;
    updateView(secondes, dizainesDeSecondes, minutes)
}

function updateView(secondes, dizainesDeSecondes, minutes) {
    secondesSpan.innerText = `${dizainesDeSecondes}${secondes}`;
    minutesSpan.innerText = minutes;
}